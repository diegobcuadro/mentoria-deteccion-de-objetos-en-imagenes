# Mentoria Detección de Objetos en Imágenes

Programa de Mentorías Diplomatura en Ciencia de Datos y sus aplicaciones 2021 - Universidad Nacional de Córdoba.

## Mentor: Diego Gonzalez Dondo

El presente repositorio contiene notebooks y datasets utilizados en el proyecto de mentoría de la Diplomatura en Ciencia de Datos de FaMAF, edición 2021.

Las notebooks fueron realizadas a partir de las notebooks de la mentoría a cargo de Lucia Benitez [Repo](https://github.com/luciabarg/mentoria_img). 

Los datos fueron obtenidos del dataset [Deuba](https://gitlab.com/ciiiutnfrc/deuba) del proyecto de detección de uso de protectores buconasales. 




## Estudiantes:

### Grupo 1: 
- Maria Roberta Devesa
- Facundo Navarro
- Juan Antonio Alvarez

Repo: [link](https://gitlab.com/mentoria-imagenes/mentoria2021-grupo-1/)


### Grupo 2: 
- Hugo Carignano
- Federico Raul Diaz
- Roxana Noelia Villafañe

Repo: [link](https://gitlab.com/mentoria-imagenes/mentoria2021-grupo-2/)

## Descripción 

La detección de objetos un proceso del área de Visión por computador, que consiste en determinar la prescencia de determinados objetos y la posición de los mismos en una imágen.

Particularmente en esta mentoría se propone trabajar con imágenes de personas trabajando utilizando elementos de protección personal (EPPs), con el objeto de determinar si las personas están utilizando los mismos. Para lo cual haremos uso de las herramientas aprendidas en la diplomatura aplicadas a imágenes.

Para la realización de esta tarea se cuenta con un conjunto de imágenes de personas etiquetadas, en diferentes condiciones de iluminación y tamaño.

## Este tema es interesante porque...

La aplicación final sería utilizar dichas imágenes para entrenar una red neuronal para la detección de los EPPs en secuencias de imágenes.

Es de interes la aplicación de estas tecnicas porque permite contar con una herramienta para la prevención de accidentes de trabajo por el incorrecto uso de los elementos de protección personal.

Es a la vez, un desafío, ya que las imágenes entran en la categoría de ‘datos no estructurados’ por lo que la mayoría de los métodos que aprenderán a lo largo de la Diplomatura deberán ser adaptados para esta clase de datos.

## Trataremos de responder algunas de las siguientes preguntas

¿Cómo realizar una exploración de datos en imágenes?

¿Cómo distinguir un objeto del fondo?

¿En qué se diferencia un enfoque clásico de procesamiento de imágenes vs redes

convolucionales (CNN) para visión por computadora?

¿Qué carácteristicas se utilizan para la representación numerica de los objetos?

## Los datos

Se cuenta con un conjunto de imágenes de personas utillizando protectores buconasales. Este conjunto de datos fue elaborado utilizando 11 diferentes videos disponibles en la plataforma YouTube, en donde se muestran personas utilizando máscaras de protección en diversas situaciones y entornos: videos de trabajadores en las industrias, personas en la vı́a pública, entrevistas a personas y niños en las escuelas.

Si querés inspeccionar el conjunto de datos, lo encontrarás en:

https://gitlab.com/ciiiutnfrc/deuba

## Hitos de la mentoría

Entrega 6/6 - Práctico de análisis y visualización,que consistirá en explorar las imágenes y extraer características con métodos tradicionales de procesamiento de imágenes. Con esto se elaborará una tabla de features, con su posterior análisis.

Entrega 4/7 - Práctico de análisis exploratorio y curación de datos, que consistirá en determinar el correcto etiquetado del conjunto de datos y aplicar diferentes filtros y extraer características de las imágenes con CNN, visualizando los diferentes filtros. Hacer comparación entre ambos métodos de extracción de características.

Entrega 4/7 - Video de presentación del proyecto y dataset

Jornada 17/7 - Discusión sobre presentación de proyectos y datasets

Entrega 15/8 - Práctico de introducción al aprendizaje automático, que consistirá en distinguir diferentes condiciones de iluminación y tamaño, posiblemente con la tabla de características hallada en el 1er práctico con métodos tradicionales, sumando una columna con las clases.

Entrega 12/9 - Práctico aprendizaje supervisado, que consistirá en aplicar redes para la clasificación de dichas imágenes, separadas en carpetas.

Entrega 2/10 - Práctico aprendizaje no supervisado, que consistirá en aplicar métodos de clustering para que las imágenes se agrupen según características en común.

Entrega 22/10 - Video de presentación de mentoría.

Jornadas 12/11 y 13/11 - Presentación de mentorías

## Licencia
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Mentoria Detección de Objetos en Imágenes</span> por <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/diegobcuadro/mentoria-deteccion-de-objetos-en-imagenes" property="cc:attributionName" rel="cc:attributionURL">Diego Gonzalez Dondo</a> se distribuye bajo una <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licencia Creative Commons Atribución 4.0 Internacional</a>.<br />Basada en una obra en <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/ciiiutnfrc/deuba y https://github.com/luciabarg/mentoria_img" rel="dct:source">https://gitlab.com/ciiiutnfrc/deuba y https://github.com/luciabarg/mentoria_img</a>.
